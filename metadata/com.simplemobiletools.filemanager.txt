Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-File-Manager
Issue Tracker:https://github.com/SimpleMobileTools/Simple-File-Manager/issues
Changelog:https://github.com/SimpleMobileTools/Simple-File-Manager/blob/HEAD/CHANGELOG.md

Auto Name:File Manager
Summary:A simple file manager for browsing and editing files and directories
Description:
A simple and clean file manager for browsing or editing your files and
directories. Contains no ads, unnecessary permissions and advanced functions.
What it provides, however, is a dark theme!
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-File-Manager

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Build:1.2,2
    commit=1.2
    subdir=app
    gradle=yes

Build:1.3,3
    commit=1.3
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.3
Current Version Code:3
